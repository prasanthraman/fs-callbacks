let fs=require("fs")

function problem1(numberOfFiles){
    let randomObj={name:"randomJson"}
    

    fs.mkdir(`${__dirname}/json-directory`,callbackForDirectoryCreation)

    //callback-1
    function callbackForDirectoryCreation(){
        console.log("Created a Directory!")
        createFiles(`randomFile${numberOfFiles}.json`,numberOfFiles)
        let creationCompleteCount=0

        function createFiles(fileName,count){
            if(count==0){
                return
            }

            fs.createWriteStream(`${__dirname}/json-directory/${fileName}`)
              .write(JSON.stringify(randomObj), (err)=>{
                if(err){
                    console.error(err)
                }else{
                    console.log("Write successfull for:", fileName)
                    creationCompleteCount++
                    if(creationCompleteCount==numberOfFiles){
                        console.log("All file Creations completed")
                        deleteFiles(`randomFile${numberOfFiles}.json`,numberOfFiles)
                    }
                }
              })
            console.log("Started creating file:",count)
            count--
            createFiles(`randomFile${count}.json`,count)
        }

        function deleteFiles(fileName,remains){
            if(remains==0){
                return
            }
            fs.unlink(`${__dirname}/json-directory/${fileName}`, (err)=>{
                if(err){
                    console.log(err)
                }else{   
                    console.log(" Deleted file:",fileName)
                }
            })
            console.log("Started Deleting file:",fileName)
            remains--
            deleteFiles(`randomFile${remains}.json`,remains)
  
        }
    }
    

}

module.exports=problem1