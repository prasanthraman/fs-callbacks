let fs = require ("fs")
let path=require ("path")

function problem2(){

    fs.readFile(path.join(__dirname,`lipsum`,`lipsum.txt`),{encoding:'utf8'},(err,data)=>{
        if(err){
            console.log(err)
        }else{
            data=(data.toUpperCase())
            //console.log(data)
            fs.writeFile(path.join(__dirname,`lipsum`,`upper-lipsum.txt`),data,(err)=>{
                if(err){
                    console.log(err)
                }else{
                    console.log("Write Success for upper-lipsum")
                    fs.writeFile(path.join(__dirname,`lipsum`,`filenames.txt`),'upper-lipsum.txt',(err)=>{
                        if(err){
                            console.log(err)
                        }else{
                            console.log("Wrote upper-lipsum.txt to filenames")
                            fs.readFile(path.join(__dirname,`lipsum`,`upper-lipsum.txt`),{encoding:'utf8'},(err,data)=>{
                                if(err){
                                    console.log(err)
                                }else{
                                    data=data.toLowerCase()
                                             .split(/(?<=[.!?])/)

                                    data=data.reduce((acc,value)=>{
                                        if(value==" "){
                                            return acc
                                        }
                                        acc+=`${value.trim()}\n`
                                        return acc
                                    },"")
                                    //console.log(data)
                                    fs.writeFile(path.join(__dirname,`lipsum`,`lower-lipsum.txt`),data,(err)=>{
                                        if(err){
                                            console.log(err)
                                        }else{
                                            console.log("Write success for lower-lipsum")
                                            fs.appendFile(path.join(__dirname,`lipsum`,`filenames.txt`),`\nlower-lipsum.txt`,{encoding:'utf8'},(err)=>{
                                                if(err){
                                                    console.log(err)
                                                }else{
                                                    console.log("Appended lower-lipsum to filenames.txt")
                                                    fs.readFile(path.join(__dirname,`lipsum`,`lower-lipsum.txt`),{encoding:'utf-8'},(err,data)=>{
                                                        if(err){
                                                            throw err
                                                        }
                                                        else{
                                                            data=data.split('\n')
                                                                     .sort()
                                                                     .join('\n')
                                                            fs.writeFile(path.join(__dirname,`lipsum`,`sorted-lipsum.txt`),data,(err)=>{
                                                                if(err){
                                                                    console.log(err)
                                                                }else{
                                                                    console.log("Write Success for sorted-lipsum")
                                                                    fs.appendFile(path.join(__dirname,`lipsum`,`filenames.txt`),`\nsorted-lipsum.txt`,(err)=>{
                                                                        if(err){
                                                                            throw err
                                                                        }else{
                                                                            console.log("Appended sorted-lipsum to filenames.txt")
                                                                            fs.readFile(path.join(__dirname,`lipsum`,`filenames.txt`),{encoding:'utf-8'},(err,data)=>{
                                                                                if(err){
                                                                                    console.log(err)
                                                                                }else{
                                                                                    let fileNames=data.split(`\n`)
                                                                                    console.log(fileNames)
                                                                                    fileNames.map((fileName)=>{
                                                                                        delefile(fileName)
                                                                                    })
                                                                                }
                                                                            })

                                                                            function delefile(fileName){
                                                                                fs.unlink(path.join(__dirname,'lipsum',fileName),(err)=>{
                                                                                    if(err){
                                                                                        console.log(err)
                                                                                    }else{
                                                                                        console.log(`Deleted file : ${fileName}`)
                                                                                    }
                                                                                })
                                                                            }
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}
module.exports=problem2